from collections import defaultdict
import cStringIO as StringIO
import csv
import datetime
from heapq import nlargest
import json
import logging
import operator
from string import punctuation
import sys
import urlparse
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import web

import backports.lzma as lzma
from bs4 import BeautifulSoup
from feedgen.feed import FeedGenerator
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import requests

DATA_FILE = '/home/ec2-user/public_html/links.csv.xz'
urls = (
    '/', 'Main',
    '/json', 'JSON',
    '/title', 'PageInfo',
    '/rss', 'Feed',
    '/summary', 'Summary',
    '/xml', 'XML',
    '/csv', 'CSV',
)

app = web.application(urls, globals(), autoreload=True)
user_agent = 'Mozilla/5.0 (Windows NT 6.3; rv:36.0)'
user_agent = user_agent + ' Gecko/20100101 Firefox/37.0'


class Summary:
    def GET(self):
        location = web.input().page
        stop_words = set(stopwords.words('english') + punctuation.split())
        response = requests.get(location, headers={'User-Agent': user_agent})
        content_type = response.headers['Content-Type']
        if not content_type.startswith('text'):
            return ''
        html = response.content.decode('utf8').encode('ascii', errors='ignore')
        soup = BeautifulSoup(html)
        wanted = list()
        for s in soup.find_all('div'):
            wanted.append(s.text.encode('ascii', errors='ignore'))
        for s in soup.find_all('p'):
            wanted.append(s.text.encode('ascii', errors='ignore'))
        text = ''.join([w for w in wanted])
        sents = sent_tokenize(text)
        word_sent = [word_tokenize(s.lower()) for s in sents]
        for word in word_sent:
            freq = defaultdict(int)
            for s in word_sent:
                for word in s:
                    if word not in stop_words:
                        freq[word] += 1

            # frequencies normalization
            m = float(max(freq.values()))
            for w in freq.keys():
                freq[w] = freq[w]/m
                if freq[w] > 0.9 or freq[w] < 0.1:
                    del freq[w]
        ranking = defaultdict(int)
        for i, sent in enumerate(word_sent):
            for w in sent:
                if w in freq:
                    ranking[i] += freq[w]
        sents_idx = nlargest(1, ranking, key=ranking.get)
        return sents[sents_idx[0]]

class XML:
    def GET(self):
        root = Element('links')
        FIELDS = ["Timestamp", "Link", "Recipient"]
        with lzma.open(DATA_FILE, mode='r') as fin:
            links_ = csv.DictReader(fin, fieldnames=FIELDS)
            for link in links_:
                link_ = SubElement(root, 'link')
                date = SubElement(link_, 'timestamp')
                date.text = link['Timestamp']
                loc = SubElement(link_, 'location')
                try:
                    loc.text = link['Link'].encode('ascii')
                except UnicodeDecodeError, e:
                    next
        web.header('Content-Type', 'text/xml')
        return '<?xml version="1.0" encoding="utf-8"?>\n{0}'.format(tostring(root))
            

class Feed:
    def GET(self):
        links = []
        FIELDS = ["Timestamp", "Link", "Recipient"]
        with lzma.open(DATA_FILE, mode='r') as fin:
            links_ = csv.DictReader(fin, fieldnames=FIELDS)
            links = list(links_)
        fg = FeedGenerator()
        fg.id('http://links.d8u.us/rss')
        fg.title("Hasan's shared links, in XML")
        fg.author(name='Hasan Diwan', email="hasandiwan@gmail.com")
        fg.link(href="http://links.d8u.us", rel='alternate')
        fg.link(href='http://links.d8u.us/rss', rel='self')
        fg.language('en')
        for entry in links[1:]:
            fe = fg.add_entry()
            URL = 'http://links.d8u.us/title?page={}'.format(entry['Link'])
            fe.title = requests.get().content(URL)
            updated__ = float(entry['Timestamp'])
            updated_ = datetime.datetime.fromtimestamp(updated__)
            fe.updated = updated_.toisoformat()
            fe.link = entry['Link']


class PageInfo:
    def GET(self):
        title = '<pre>Not a webpage</pre>'
        i = web.input()
        page = i.page
        logging.debug('/title invoked with page {0}!'.format(page))

        status = 999
        while status > 400:
            contents = requests.get(page, verify=False)
            status = contents.status_code
            if status == 403:
                return page
        soup = BeautifulSoup(contents.content)

        if soup.title is None:
            return page

        title = soup.title.string
        logging.debug("{0}'s title is '{1}'".format(page, title))
        return title


class CSV:
    def GET(self):
        CSV.fields = ["Timestamp", "Link", "Recipient"]
        with lzma.open(DATA_FILE, mode='r') as fin:
            links_ = csv.DictReader(fin, fieldnames=CSV.fields)
            links = list(links_)
        out = StringIO.StringIO()
        web.header('Content-Type', 'text/csv')
        writer = csv.DictWriter(out, fieldnames=CSV.fields, quoting=csv.QUOTE_ALL)
        writer.writerows(links)
        return out.getvalue()

class JSON:
    def GET(self):
        dumped = []
        fields = ["Timestamp", "Link", "Recipient"]
        with lzma.open(DATA_FILE, mode='r') as fin:
            links_ = csv.DictReader(fin, fieldnames=fields)
            for l in list(links_)[1:]:
                dump_of_item = {}
                for field in links_.fieldnames:
                    dump_of_item[field] = l[field]
                dumped.append(dump_of_item)
        web.header('Content-Type', 'application/json')
        return json.dumps({'links':dumped})


class Main:
    (TIMESTAMP_INDEX, LINK_INDEX, RECIPIENT_INDEX) = (0, 1, 2)

    def GET(self):
        relevant = []
        seen = set()
        with lzma.open(DATA_FILE) as fin:
            links = list(csv.reader(fin))[1:]
            for r in links:
                link = r[Main.LINK_INDEX]
                recipient = r[Main.RECIPIENT_INDEX]
                timestamp = float(r[Main.TIMESTAMP_INDEX])
                if r[Main.LINK_INDEX] not in seen:
                    iso_timestamp_ = datetime.datetime.fromtimestamp(timestamp)
                    iso_timestamp = iso_timestamp_.isoformat()
                    parsed = urlparse.urlparse(link)
                    favicon = '{0}://{1}/favicon.ico'.format(parsed.scheme, parsed.netloc)
                    line = [iso_timestamp, link, recipient, favicon]
                    relevant.append(line)
                seen.add(link)
        render = web.template.render('/home/ec2-user/sharedViewer/templates')
        r = render.hello(links=reversed(relevant))
        return r

    def POST(self):
        parameters = web.input()
        params = {'units': 'metric'}
        try:
            params['lat'] = parameters.lat
            params['lon'] = parameters.lng
        except AttributeError:
            params['zip'] = '94112,us'
        logging.debug('Parameters for openweathermap are {0}'.format(params))
        URL = 'http://api.openweathermap.org/data/2.5/weather'
        response = requests.get(URL, params=params)
        stanza = response.json()
        return stanza['main']['temp']


if __name__ == '__main__':
    log_format = '%(asctime)s %(levelname)s %(module)s.%(funcName)s %(message)'
    log_format = log_format + 's'
    date_format = '%Y-%m-%dT%H:%M:%S'
    log_lvl = logging.DEBUG
    logging.basicConfig(level=log_lvl, format=log_format, datefmt=date_format)

    # according to http://stackoverflow.com/a/25714107/783412
    # this forces reloading
    web.config.debug = True
    sys.argv.append('58001')
    app.run()
